//Creates variables for elements I want to interact with. 
//Declares them in one place

const bankBalanceElement = document.getElementById('bankBalance');
const loanBalanceElement = document.getElementById('loanBalance');
const getLoanButtonElement = document.getElementById('getLoan');

//2. work
const bankButtonElement = document.getElementById('bankButton');
const payBalanceElement = document.getElementById('payBalance');
const workButtonElement = document.getElementById('workButton');
const repayLoanButtonElement = document.getElementById('repayLoan');
//3
const laptopsElement = document.getElementById('laptops');
const featuresElement = document.getElementById('features');

//declaring elements that are going to change as let variables
let bankBalance = 200;
let loanBalance = 0.0;
//2
let payBalance = 0.0;
//3
let laptops = [];
//4
const apiURL = "https://noroff-komputer-store-api.herokuapp.com/computers";
const imageUrl = "https://noroff-komputer-store-api.herokuapp.com/";
const titleElement = document.getElementById('title');
let priceElement = document.getElementById('price');
const descriptionElement = document.getElementById('description');
const buyNowButtonElement = document.getElementById('buyNow');
let imageElement = document.getElementById('image');

//fetching laptops from Api
fetch(apiURL)
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToStore(laptops))

//Funciton for adding laptops specs to the features Element
const addLaptopsToStore = (laptops) => {
    laptops.forEach(x => addLaptopToStore(x));
    featuresElement.innerText = laptops[0].specs;
    titleElement.innerText = laptops[0].title;
    priceElement.innerText = laptops[0].price;
    descriptionElement.innerText = laptops[0].description;
    imageElement.src = imageUrl+laptops[0].image;
}

//Creating a option element in the dropdownbox for each laptop
const addLaptopToStore = (laptop) => {
    const laptopElement = document.createElement('option');
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

//Changing the information displayed based on what laptop is selected
const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    featuresElement.innerText = selectedLaptop.specs;
    titleElement.innerText = selectedLaptop.title;
    priceElement.innerText = selectedLaptop.price;
    descriptionElement.innerText = selectedLaptop.description;
    imageElement.src = imageUrl+selectedLaptop.image;
}

//get loan prompt
const handleGetLoan = () => {
    const askingLoan = prompt("How much do you want to borrow? ");

    //checking if loan request is ok
   const ratio = askingLoan / bankBalance;
    if (ratio > 2.001) {
        alert(`Loan request denied. Your request is more than double of your bank balance`)
    } else if (loanBalance > 0) {
        alert(`Loan request denied. You already have a loan `)
    } else {
        loanBalance = askingLoan;
        loanBalanceElement.innerText = `Loan balance: ${loanBalance} kr`;
    }
}

//Increasing the pay balance by 100 each time "work" is pressed. updating the html and the variable
const handleWork = () => {
    payBalance += 100;
    payBalanceElement.innerText = `${payBalance}`;
}

//Taking banking requests and doing calculations on the pay balance and subtraction loan, adding bank
const handleBank = () => {
    let transferToBank = 0;
    let payLoan = 0;
    payLoan = payBalance * 0.1;
    transferToBank = payBalance-payLoan;
    loanBalance -= payLoan;
    //cheking if the user has an active loan
    if (loanBalance>0){
        loanBalanceElement.innerHTML = `Loan Balance: ${loanBalance} kr`
        bankBalanceElement.innerHTML = `${bankBalance+=transferToBank}`
        payBalance = 0;
        payBalanceElement.innerHTML = `${payBalance}`;
    } else {
        bankBalanceElement.innerHTML = `${bankBalance+=payBalance}`
        payBalance = 0;
        payBalanceElement.innerHTML = `${payBalance}`;
    }
}

//function for paying back loan. 
const handleLoan = () => {
    let repayLoanLeftover= 0;
    repayLoanLeftover = payBalance - loanBalance;
    //checking if the pay balance is larger than the loan balance
    //if so, the remaining part is transferred to bank balance
    if(repayLoanLeftover > 0) {
        bankBalanceElement.innerHTML = `${bankBalance+=repayLoanLeftover}`
        loanBalance = 0;
        loanBalanceElement.innerHTML = `Loan Balance: ${loanBalance} kr`
        payBalance = 0;
        payBalanceElement.innerHTML = `${payBalance}`;
    } else {
        loanBalanceElement.innerHTML = `Loan Balance: ${loanBalance-=payBalance} kr`
        payBalance = 0;
        payBalanceElement.innerHTML = `${payBalance}`;
    }
}
 
const handleBuyNow = () => {
    let price = priceElement.innerHTML;
   if(bankBalance < price){
       alert(`You don't have enough money`);
   } else {
       bankBalanceElement.innerHTML = `${bankBalance -= price}`
       alert(`Congratulations! You are now the owner of a new laptop`)
   }
}


//Activating the buttons
getLoanButtonElement.addEventListener("click", handleGetLoan);
workButtonElement.addEventListener("click", handleWork);
bankButtonElement.addEventListener("click", handleBank);
repayLoanButtonElement.addEventListener("click", handleLoan);
laptopsElement.addEventListener("change", handleLaptopChange);
buyNowButtonElement.addEventListener("click", handleBuyNow);